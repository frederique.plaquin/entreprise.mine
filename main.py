from Simulation import Simulation
import Variables
from Graphs import BaseGraph
import kivy
kivy.require('2.0.0')  # replace with your current kivy version !

from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button


class LancerSimulation:
    def __init__(self, input_value):
        nb_mois = input_value
        # if type(nb_mois) is not int:
            # raise WrongTypeError("La période d'activité doit être exprimée en nombres (int).")
        print("=====", nb_mois)
        # simulation entreprise n°1
        Simulation1 = Simulation()
        entreprise1 = "Marchad"
        chef = {"nom": "Dupont", "prenom": "Jean", "sexe": "homme", "age": "45", "salaire": 2800,
                "voiture_fonction": True,
                "voiture_personnelle": 1, "num_securite_sociale": "1255896574"}
        Simulation1.jeu_donnees(chef, entreprise1, Variables.liste_employables)
        Simulation1.animer(int(nb_mois))
        # simulation entreprise n°2
        Simulation2 = Simulation()
        entreprise2 = "Fludubux"
        chef2 = {"nom": "Tartenpion", "prenom": "Cécile", "sexe": "femme", "age": "38", "salaire": 2900,
                 "voiture_fonction": True, "voiture_personnelle": 1, "num_securite_sociale": "2255896574"}
        Simulation2.jeu_donnees(chef2, entreprise2, Variables.liste_employables2)
        Simulation2.animer(int(nb_mois))
        # affichage graphiques
        graph = BaseGraph(int(nb_mois))
        graph.show(Simulation1.entreprise, Simulation1.entreprise.charges_annuelles,
                   Simulation1.entreprise.achats_annuels,
                   Simulation2.entreprise, Simulation2.entreprise.charges_annuelles,
                   Simulation2.entreprise.achats_annuels)


class InterfaceGraphique(GridLayout):
    def __init__(self, **kwargs):
        super(InterfaceGraphique, self).__init__(**kwargs)
        self.cols = 1
        self.intro_text = Label(text="Bienvenue sur SimuLife, l' application qui simule la vie de votre entreprise.")
        self.add_widget(self.intro_text)
        self.grid = GridLayout(size_hint_y=0.2)
        self.grid.cols = 2
        self.label = Label(text="Choisissez une période d'activité : ")
        self.grid.add_widget(self.label)
        self.input = TextInput(multiline=False)
        self.grid.add_widget(self.input)
        self.add_widget(self.grid)
        self.start_simulation = Button(text="Lancer la simulation")
        self.add_widget(self.start_simulation)
        self.start_simulation.bind(on_press=self.callback)

    def callback(self, instance):
        input_value = self.input.text
        LancerSimulation(input_value)


class WrongTypeError(Exception):
    pass

class MyApp(App):

    def build(self):
        return InterfaceGraphique()


if __name__ == '__main__':
    MyApp().run()