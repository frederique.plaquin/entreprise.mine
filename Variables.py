liste_employables = [
    {
        "nom": "Guezenec",
        "prenom": "Yuna",
        "sexe": "femme",
        "age": 25,
        "num_securite_sociale": "23345434555",
        "salaire": 1700,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Germain",
        "prenom": "Paul",
        "sexe": "homme",
        "age": 55,
        "num_securite_sociale": "13345434555",
        "salaire": 1550,
        "voiture_fonction": 0,
        "voiture_personnelle": 2
    },
    {
        "nom": "Truhe",
        "prenom": "Carole",
        "sexe": "femme",
        "age": 26,
        "num_securite_sociale": "233454358855",
        "salaire": 1250,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Gruth",
        "prenom": "Jesappelle",
        "sexe": "homme",
        "age": 140,
        "num_securite_sociale": "03345434555",
        "salaire": 1350,
        "voiture_fonction": 0,
        "voiture_personnelle": 0
    },
    {
        "nom": "Dubreuil",
        "prenom": "Jeanne",
        "sexe": "femme",
        "age": 24,
        "num_securite_sociale": "2334534555",
        "salaire": 1750,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Troll",
        "prenom": "Karlow",
        "sexe": "homme",
        "age": 21,
        "num_securite_sociale": "1338644555",
        "salaire": 2000,
        "voiture_fonction": 0,
        "voiture_personnelle": 4
    },
    {
        "nom": "Jules",
        "prenom": "Verne",
        "sexe": "homme",
        "age": 58,
        "num_securite_sociale": "13345434555",
        "salaire": 1950,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Veillo",
        "prenom": "Chuliane",
        "sexe": "femme",
        "age": 27,
        "num_securite_sociale": "2648134555",
        "salaire": 1750,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Chevalier",
        "prenom": "Maurice",
        "sexe": "homme",
        "age": 42,
        "num_securite_sociale": "192184926255",
        "salaire": 1420,
        "voiture_fonction": 0,
        "voiture_personnelle": 0
    },
    {
        "nom": "Truts",
        "prenom": "Louise",
        "sexe": "femme",
        "age": 24,
        "num_securite_sociale": "2514186529",
        "salaire": 1800,
        "voiture_fonction": 0,
        "voiture_personnelle": 2
    },
    {
        "nom": "Jackson",
        "prenom": "Mickael",
        "sexe": "homme",
        "age": 57,
        "num_securite_sociale": "13561984651655",
        "salaire": 1300,
        "voiture_fonction": 0,
        "voiture_personnelle": 2
    },
    {
        "nom": "Tab",
        "prenom": "Jack",
        "sexe": "homme",
        "age": 21,
        "num_securite_sociale": "132654651652*",
        "salaire": 1550,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Tartouille",
        "prenom": "Jess",
        "sexe": "femme",
        "age": 47,
        "num_securite_sociale": "2654784555",
        "salaire": 1930,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    }
]

liste_employables2 = [
    {
        "nom": "Guezenec",
        "prenom": "Yuna",
        "sexe": "femme",
        "age": 25,
        "num_securite_sociale": "23345434555",
        "salaire": 1700,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Germain",
        "prenom": "Paul",
        "sexe": "homme",
        "age": 55,
        "num_securite_sociale": "13345434555",
        "salaire": 1550,
        "voiture_fonction": 0,
        "voiture_personnelle": 2
    },
    {
        "nom": "Truhe",
        "prenom": "Carole",
        "sexe": "femme",
        "age": 26,
        "num_securite_sociale": "233454358855",
        "salaire": 1250,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Gruth",
        "prenom": "Jesappelle",
        "sexe": "homme",
        "age": 140,
        "num_securite_sociale": "03345434555",
        "salaire": 1350,
        "voiture_fonction": 0,
        "voiture_personnelle": 0
    },
    {
        "nom": "Dubreuil",
        "prenom": "Jeanne",
        "sexe": "femme",
        "age": 24,
        "num_securite_sociale": "2334534555",
        "salaire": 1750,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Troll",
        "prenom": "Karlow",
        "sexe": "homme",
        "age": 21,
        "num_securite_sociale": "1338644555",
        "salaire": 2000,
        "voiture_fonction": 0,
        "voiture_personnelle": 4
    },
    {
        "nom": "Jules",
        "prenom": "Verne",
        "sexe": "homme",
        "age": 58,
        "num_securite_sociale": "13345434555",
        "salaire": 1950,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Veillo",
        "prenom": "Chuliane",
        "sexe": "femme",
        "age": 27,
        "num_securite_sociale": "2648134555",
        "salaire": 1750,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Chevalier",
        "prenom": "Maurice",
        "sexe": "homme",
        "age": 42,
        "num_securite_sociale": "192184926255",
        "salaire": 1420,
        "voiture_fonction": 0,
        "voiture_personnelle": 0
    },
    {
        "nom": "Truts",
        "prenom": "Louise",
        "sexe": "femme",
        "age": 24,
        "num_securite_sociale": "2514186529",
        "salaire": 1800,
        "voiture_fonction": 0,
        "voiture_personnelle": 2
    },
    {
        "nom": "Jackson",
        "prenom": "Mickael",
        "sexe": "homme",
        "age": 57,
        "num_securite_sociale": "13561984651655",
        "salaire": 1300,
        "voiture_fonction": 0,
        "voiture_personnelle": 2
    },
    {
        "nom": "Tab",
        "prenom": "Jack",
        "sexe": "homme",
        "age": 21,
        "num_securite_sociale": "132654651652*",
        "salaire": 1550,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    },
    {
        "nom": "Tartouille",
        "prenom": "Jess",
        "sexe": "femme",
        "age": 47,
        "num_securite_sociale": "2654784555",
        "salaire": 1930,
        "voiture_fonction": 0,
        "voiture_personnelle": 1
    }
]
