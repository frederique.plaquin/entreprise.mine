from Personnes.Salarie import Salarie
from Personnes.Directeur import Directeur
from Voiture import Voiture
from Bureau import Bureau
import random


class Entreprise:
    def __init__(self, nom, directeur):
        self.nom = nom
        self.salaries = []
        self.bureaux = []
        self.voitures = []
        self.ca_mensuel = 0
        self.charges_mensuelles = 0
        self.achats_mensuels = 0
        self.ventes_mensuelles = 0
        self.ca_annuel = []
        self.charges_annuelles = []
        self.achats_annuels = []
        self.ventes_annuelles = []
        self.directeur = Directeur(directeur["nom"], directeur["prenom"], directeur["sexe"], directeur["age"],
                                   self.nom, directeur["salaire"], directeur["voiture_fonction"],
                                   directeur["voiture_personnelle"], directeur["num_securite_sociale"])

    def remise_a_zero(self):
        self.ca_mensuel = 0
        self.charges_mensuelles = 0
        self.achats_mensuels = 0
        self.ventes_mensuelles = 0

    def embaucher(self, nb_salaries, employables):
        print("nb salariés à embaucher : ", nb_salaries)
        nb_salaries_embauches = 0
        if len(employables) != 0:
            if nb_salaries <= len(employables):
                limite = nb_salaries
            else:
                limite = len(employables)
            for i in range(limite):
                nom = employables[0]["nom"]
                prenom = employables[0]["prenom"]
                sexe = employables[0]["sexe"]
                age = employables[0]["age"]
                employeur = self.nom
                salaire = employables[0]["salaire"]
                voiture_fonction = employables[0]["voiture_fonction"]
                voiture_personnelle = employables[0]["voiture_personnelle"]
                num_securite_sociale = employables[0]["num_securite_sociale"]
                salarie = Salarie(nom, prenom, sexe, age, employeur, salaire, voiture_fonction, voiture_personnelle,
                                  num_securite_sociale)
                self.salaries.append(salarie)
                employables.remove(employables[0])
                nb_salaries_embauches += 1
        print('nb salariés embauchés : ', nb_salaries_embauches)

    def licencier(self, nb_salaries, employables):
        nb_salaries_entreprise = len(self.salaries)
        print("nb salariés à licencier : ", nb_salaries)
        print('nb voitures avant licenciement : ', len(self.voitures))
        nb_salaries_licencies = 0
        for i in range(nb_salaries):
            if i <= nb_salaries_entreprise:
                nom = self.salaries[0].nom
                prenom = self.salaries[0].prenom
                sexe = self.salaries[0].sexe
                age = self.salaries[0].age
                employeur = ''
                salaire = self.salaries[0].salaire
                voiture_fonction = 0
                voiture_personnelle = self.salaries[0].voiture_personnelle
                num_securite_sociale = self.salaries[0].num_securite_sociale
                salarie = {"nom": nom, "prenom": prenom, "sexe": sexe, "age": age, "employeur": employeur,
                           "salaire": salaire, "voiture_fonction": voiture_fonction,
                           "voiture_personnelle": voiture_personnelle, "num_securite_sociale": num_securite_sociale}
                employables.append(salarie)
                for item in self.voitures:
                    if item.proprietaire.num_securite_sociale == salarie['num_securite_sociale']:
                        self.ventes_mensuelles += item.prix
                        self.voitures.remove(item)
                self.salaries.remove(self.salaries[0])
                nb_salaries_licencies += 1
                if self.bureaux:
                    self.bureaux[0].libre = True
        print('nombre salariés licenciés : ', nb_salaries_licencies)
        print('nb voitures après licenciement : ', len(self.voitures))

    def acheter_bien(self, nb_biens, type_bien):
        if type_bien == "voiture":
            print("Nb de ", type_bien, " avant achat : ", len(self.voitures))
            print("Nb de ", type_bien, " à acheter : ", nb_biens)
            nb_voitures_achetees = 0
            for i in range(nb_biens):
                stop = False
                for item in self.salaries:
                    if not stop:
                        if item.voiture_fonction == 0:
                            voiture = Voiture(item)
                            self.voitures.append(voiture)
                            item.voiture_fonction = 1
                            self.achats_mensuels += voiture.prix
                            stop = True
                            nb_voitures_achetees += 1
            print('nb voitures achetées : ', nb_voitures_achetees)
        elif type_bien == "bureau":
            print("Nb de ", type_bien, " avant achat : ", len(self.bureaux))
            print("Nb de ", type_bien, " à acheter : ", nb_biens)
            nb_bureaux_achetes = 0
            for i in range(nb_biens):
                if len(self.bureaux) < len(self.salaries):
                    bureau = Bureau()
                    self.bureaux.append(bureau)
                    self.achats_mensuels += bureau.prix
                    nb_bureaux_achetes += 1
            print('nb bureaux achetées : ', nb_bureaux_achetes)
        else:
            print("Nous n' achetons que des voitures et des bureaux. Merci de votre compréhension.")

    def vendre_bien(self, nb_biens, type_bien):
        if type_bien == "voiture":
            print("Nb de ", type_bien, " avant vente : ", len(self.voitures))
            print("Nb ", type_bien, " à vendre : ", nb_biens)
            nb_voitures_vendues = 0
            if nb_biens <= len(self.voitures):
                limite = nb_biens
            else:
                limite = len(self.voitures)
            for i in range(limite):
                voiture_a_vendre = self.voitures[0]
                proprietaire = voiture_a_vendre.proprietaire
                self.voitures.remove(voiture_a_vendre)
                self.ventes_mensuelles += voiture_a_vendre.prix
                if proprietaire:
                    for item in self.salaries:
                        if item == proprietaire:
                            salarie = item
                            salarie.voiture_fonction = 0
                nb_voitures_vendues += 1
            print('nb voitures vendues : ', nb_voitures_vendues)
        elif type_bien == "bureau":
            bureaux_libres = []
            for item in self.bureaux:
                if item.libre:
                    bureaux_libres.append(item)
            (print("nombre bureaux libres : ", len(bureaux_libres)))
            print("Nb de ", type_bien, " avant vente : ", len(self.bureaux))
            print("Nb de ", type_bien, " à vendre : ", nb_biens)
            nb_bureaux_vendus = 0
            if nb_biens <= len(self.bureaux):
                limite = nb_biens
            else:
                limite = len(self.bureaux)
            for i in range(limite):
                stop = False
                for item in self.bureaux:
                    if not stop:
                        if item.libre:
                            self.bureaux.remove(item)
                            self.ventes_mensuelles += item.prix
                            nb_bureaux_vendus += 1
                            stop = True
            print('nb bureaux vendus : ', nb_bureaux_vendus)
        else:
            print("Nous ne vendons que des voitures et des bureaux. Merci de votre compréhension.")

    def calcul_ca_mensuel(self):
        self.ca_mensuel = random.randrange(100000, 500000)

    def calcul_charges_mensuelles(self):
        for item in self.salaries:
            self.charges_mensuelles += item.salaire
        self.charges_mensuelles += self.directeur.salaire
        for item in self.voitures:
            self.charges_mensuelles += item.cout_entretien_mensuel

    def nommer_directeur(self):
        if len(self.salaries) != 0:
            salarie = random.choice(self.salaries)
            nom = salarie.nom
            prenom = salarie.prenom
            sexe = salarie.sexe
            age = salarie.age
            employeur = salarie.employeur
            salaire = salarie.salaire
            voiture_fonction = salarie.voiture_fonction
            voiture_personnelle = salarie.voiture_personnelle
            num_securite_sociale = salarie.num_securite_sociale
            directeur = Directeur(nom, prenom, sexe, age, employeur, salaire, voiture_fonction, voiture_personnelle,
                                  num_securite_sociale)
            self.directeur = directeur

    def sauvegarde_activite_mensuelle(self):
        self.ca_annuel.append(self.ca_mensuel)
        self.charges_annuelles.append(self.charges_mensuelles)
        self.achats_annuels.append(self.achats_mensuels)
        self.ventes_annuelles.append(self.ventes_mensuelles)

    def afficher_entreprise(self):
        chef_nom = self.directeur.nom
        chef_prenom = self.directeur.prenom
        print("Nom de l'entreprise : ", self.nom)
        print("Directeur : ", chef_prenom, chef_nom)
        print("Nombre de salariés dans l' entreprise : ", len(self.salaries))
        print("Nombre de bureaux : ", len(self.bureaux))
        print("Nombre de voitures : ", len(self.voitures))
        print("Chiffre d' affaires mensuel : ", self.ca_mensuel)
        print("Charges mensuelles : ", self.charges_mensuelles)
        print("Achats mensuels : ", self.achats_mensuels)
        print("Ventes mensuelles : ", self.ventes_mensuelles)
        benefices_mensuelles = (self.ca_mensuel + self.ventes_mensuelles) - (self.charges_mensuelles +
                                                                             self.achats_mensuels)
        print("Bénéfices mensuelles : ", benefices_mensuelles)
        print("Chiffre d' affaires annuel : ", sum(self.ca_annuel))
        print("Charges annuelles : ", sum(self.charges_annuelles))
        print("Achats annuels : ", sum(self.achats_annuels))
        print("Ventes annuelles : ", sum(self.ventes_annuelles))
        benefices_annuels = (sum(self.ca_annuel) + sum(self.ventes_annuelles)) - (sum(self.charges_annuelles) +
                                                                                  sum(self.achats_annuels))
        print("Bénéfices annuels : ", benefices_annuels)

    def afficher_embauches(self):
        print("Nombre de salariés après embauche dans l' entreprise : ", len(self.salaries))

    def afficher_licenciement(self):
        print("Nombre de salariés après licenciement dans l' entreprise : ", len(self.salaries))

    def afficher_achats(self):
        print("Nombre de bureaux après achats : ", len(self.bureaux))
        print("Nombre de voitures après achats : ", len(self.voitures))

    def afficher_ventes(self):
        print("Nombre de bureaux après ventes : ", len(self.bureaux))
        print("Nombre de voitures après ventes : ", len(self.voitures))
