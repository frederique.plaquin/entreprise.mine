import random


class Voiture:
    def __init__(self, proprietaire=""):
        self.proprietaire = proprietaire
        self.prix = random.randrange(10000, 20000)
        self.cout_entretien_mensuel = random.randrange(200, 1000)
