import random
from Entreprise import Entreprise
from Personnes.Directeur import Directeur


class Simulation:
    def __init__(self):
        self.chef = None
        self.entreprise = None
        self.liste_employables = []

    def jeu_donnees(self, chef, entreprise, liste_employables):
        chef_nom = chef["nom"]
        chef_prenom = chef["prenom"]
        chef_sexe = chef["sexe"]
        chef_age = chef["age"]
        chef_employeur = entreprise
        chef_salaire = chef["salaire"]
        chef_voiture_fonction = chef["voiture_fonction"]
        chef_voiture_perso = chef["voiture_personnelle"]
        chef_num_securite_sociale = chef["num_securite_sociale"]
        self.chef = Directeur(chef_nom, chef_prenom, chef_sexe, chef_age, chef_employeur, chef_salaire,
                              chef_voiture_fonction, chef_voiture_perso, chef_num_securite_sociale)
        self.entreprise = Entreprise(entreprise, chef)
        self.liste_employables = liste_employables

    def animer(self, nb_mois):
        if nb_mois <= 0 or nb_mois > 120:
            raise PeriodOutOfScopeError("La période d'activité doit être comprise entre 1 et 120 inclus.")
        print("############################## Création entreprise ################################")
        self.entreprise.embaucher(random.randrange(0, 10), self.liste_employables)
        self.entreprise.nommer_directeur()
        self.entreprise.afficher_entreprise()
        for i in range(0, nb_mois):
            self.entreprise.remise_a_zero()
            print("############################## Embauches ################################")
            self.entreprise.embaucher(random.randrange(0, 10), self.liste_employables)
            self.entreprise.afficher_embauches()
            print("############################# Licenciements ####################################")
            if len(self.entreprise.salaries) == 0:
                print("Pas de salarié à licencier ! ")
            else:
                self.entreprise.licencier(random.randrange(0, len(self.entreprise.salaries)), self.liste_employables)
            self.entreprise.afficher_licenciement()
            print("############################ Augmentations salaire ##############################")
            self.chef.augmenter_salaire(self.entreprise.salaries)
            print("############################## Achats #####################################")
            if len(self.entreprise.salaries) == 0:
                print("Nous n' avons pas besoin de voitures!")
            else:
                self.entreprise.acheter_bien(random.randrange(0, len(self.entreprise.salaries)), "voiture")
            if len(self.entreprise.salaries) == 0:
                print("Nous n' avons pas besoin de bureaux!")
            else:
                self.entreprise.acheter_bien(random.randrange(0, len(self.entreprise.salaries)), "bureau")
            self.entreprise.afficher_achats()
            print("############################### Ventes ######################################")
            if len(self.entreprise.voitures) == 0:
                print("Pas de voitures à vendre ! ")
            else:
                self.entreprise.vendre_bien(random.randrange(0, len(self.entreprise.voitures)), "voiture")
            if len(self.entreprise.bureaux) == 0:
                print("Pas de bureaux à vendre ! ")
            else:
                self.entreprise.vendre_bien(random.randrange(0, len(self.entreprise.bureaux)), "bureau")
            self.entreprise.afficher_ventes()
            self.entreprise.calcul_ca_mensuel()
            self.entreprise.calcul_charges_mensuelles()
            print("############################### Bilan activité Mensuelle ######################################")
            self.entreprise.afficher_entreprise()
            print("################## Sauvegarde activité depuis la création de l' entreprise #######################")
            self.entreprise.sauvegarde_activite_mensuelle()
            self.entreprise.afficher_entreprise()


class PeriodOutOfScopeError(Exception):
    pass
