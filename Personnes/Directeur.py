import random

from Personnes import Salarie


class Directeur(Salarie.Salarie):
    def __init__(self, nom, prenom, sexe, age, employeur, salaire, voiture_fonction, voiture_personnelle,
                 num_securite_sociale):
        Salarie.Salarie.__init__(self, nom, prenom, sexe, age, employeur, salaire, voiture_fonction,
                                 voiture_personnelle, num_securite_sociale)

    def augmenter_salaire(self, salaries):
        if len(salaries) != 0:
            nombre_salaries_a_augmenter = random.randrange(len(salaries))
            i = 0
            deja_augmente = []
            while i <= nombre_salaries_a_augmenter and i <= len(salaries):
                i += 1
                salarie = random.choice(salaries)
                if salarie not in deja_augmente:
                    augmentation = random.randrange(50, 200)
                    salarie.salaire += augmentation
                    deja_augmente.append(salarie)
                    print(salarie.prenom, salarie.nom, " : ", augmentation)
