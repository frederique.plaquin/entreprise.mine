from Personnes import Personne


class Salarie(Personne.Personne):
    def __init__(self, nom, prenom, sexe, age, employeur, salaire, voiture_fonction, voiture_personnelle,
                 num_securite_sociale):
        Personne.Personne.__init__(self, nom, prenom, sexe, age)
        self.employeur = employeur
        self.salaire = salaire
        self.voiture_fonction = voiture_fonction
        self.voiture_personnelle = voiture_personnelle
        self.num_securite_sociale = num_securite_sociale
