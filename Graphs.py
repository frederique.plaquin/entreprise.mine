import matplotlib.pyplot as plt


class BaseGraph:

    def __init__(self, nb_mois):
        self.title = "Charges et achats sur " + str(nb_mois) + " mois - "
        self.x_label = "Mois de l' année"
        self.y_label = "Sommes en euros"
        self.show_grid = True

    def show(self, entreprise1, charges1, achats1, entreprise2, charges2, achats2):
        plt.subplot(211)
        plt.grid(self.show_grid)
        plt.title(self.title + entreprise1.nom)
        plt.plot(charges1, "b", marker=".", label="Charges")
        plt.plot(achats1, marker=".", label="achats")
        plt.ylabel(self.y_label)
        plt.xlabel(self.x_label)
        plt.legend()

        plt.subplot(212)
        plt.grid(self.show_grid)
        plt.title(self.title + entreprise2.nom)
        plt.plot(charges2, marker=".", label="Charges")
        plt.plot(achats2, marker=".", label="achats")
        plt.ylabel(self.y_label)
        plt.xlabel(self.x_label)
        plt.legend()

        plt.show()
